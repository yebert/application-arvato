package application_arvato.tasks;

import java.nio.file.Paths;

// Aufgabe 1
public class App 
{
    public static void main( String[] args ){
    	String localDir = System.getProperty("user.dir");
        FileListToJson.analyzeFiles(Paths.get(localDir, "list1.txt"), Paths.get(localDir, "list2.txt"));
    }
}
