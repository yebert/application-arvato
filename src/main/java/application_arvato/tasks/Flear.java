package application_arvato.tasks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
// Aufagbe 3
//=======================================================================
/**
 * The class Flear represents fleas that are available for purchase.
 * @author Yannis Alexander Ebert
 *
 */
public class Flear {
  //=======================================================================
	// added class variable 
  // helper variable to give new flea-types a price. 
  // In this implementation first entry is matched to "rating 1"-fleas, last to "rating 10"-fleas. 
  // But the order is irrelevant for the proper functioning of the getOptimalValue-method.
	static float[] priceList = {1.3f,1.7f,3.7f,4.6f,5.5f,6.4f,7.3f,8.2f,9.1f,10.0f};
	
	//=======================================================================
	// fields
	String name;
	float price;
	int rating;
	double ratio;  // added field to show the proportion of price per rating-point
	
	//=======================================================================
	//Constructor
	public Flear(final String name, final float price, final int rating) {
		this.name=name;
		this.price=price;
		this.rating=rating;
		setRatio();
	}
	//=======================================================================
	//Getters and Setters
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
		setRatio();
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
		setRatio();
	}
	 
  public double getRatio() {
    return ratio;
  }
  private void setRatio() {
    ratio=this.price/this.rating;
  }
	
  //=======================================================================
  // toString()
	@Override
	public String toString() {
		return "Flear [name=" + name + ", price=" + price + ", rating=" + rating + ", ratio= "+ ratio+"]";
	}
	
	//=======================================================================
	// equals and hashCode
	public boolean equals(Object object) {
		if(this==object) return true;
		if(object==null || getClass() != object.getClass()) return false;
		if(!super.equals(object)) return false;
		else return true;
	}
	public int hashCode(){
		return Objects.hash(super.hashCode(), name);
	}
	
	//=======================================================================
	/**
	 * This method calculates the optimal combination of fleas to get the highest possible sum of rating-points with a given amount of money.
	 * The method returns the optimal total and fills the passed List<Flears> with the optimal combination of fleas.
	 * Assumptions: - All fleas with the same rating have the same price.
	 *              - There are theoretically unlimited fleas of each type available
	 *              - The given money does not need to be spent completely (There can be a remainder of less than the cheapest flea)
	 * @param money
	 * @param flears
	 * @return int res
	 */
	public static int getOptimalValue(float money, List<Flear> fleas) {
		int res = 0;
		// Fill a ArrayList with all available flea-types
		List<Flear> fleaList = new ArrayList<>();
		for (int rate = 1; rate<=10;rate++) {
			fleaList.add(new Flear(rate+"er-Floh",priceList[rate-1], rate)); // match price and rating to the 10 different types of fleas
		}
		//Sort the flea-type collection by ratio
		Collections.sort(fleaList,(x,y)->((Double)x.getRatio()).compareTo(((Double)y.getRatio())));
		
		// First try to add fleas with the lowest price/rating-ratio until there isn't enough money left for this flea-type.
		// Then try to add fleas with the next best ratio and so on until there is not enough money left to buy any addional flea.
		// Idea behind this algorithm is, that you get the optimal amount of rating-points, if you add fleas with the lowest possible price per rating-point.
			for(int pos = 0;pos<10;pos++) {
				float price = fleaList.get(pos).getPrice();
				while(money>=price) {
					fleas.add(fleaList.get(pos));
					money-=price;
				}
				// Quit the for-loop if the money is less than the price of the cheapest flea
				if (money<priceList[0]) break;
			}
		// sum up the rating-points from the flea collection.
		res = fleas.stream().mapToInt(Flear::getRating).sum();
		
		return res;
	}
	
	//=======================================================================
	// Testing the getOptimalValue-method
	public static void main(String[] args) {
		List<Flear> purchase = new ArrayList<>();
		int points = getOptimalValue(22.0f,purchase);
		
		// print the results
		System.out.println("Your optimal flea combination:");
		for(Flear floehe:purchase) {
			System.out.println(floehe);
		}
		System.out.println("Total Rating: "+points);
	}
}
