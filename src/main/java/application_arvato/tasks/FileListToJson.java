package application_arvato.tasks;

import java.io.BufferedReader;

import java.io.FileReader;
import java.io.IOException;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

// Aufgabe 1
public class FileListToJson {
  //=======================================================================
	/**
	 * Static method to create a Json-Object that contains the Json-Arrays as a result of analyzing the two passed Files
	 * @param file1
	 * @param file2
	 */
 public static void analyzeFiles(Path file1, Path file2) {
	 //Using HashSets to eliminate potential duplicats
	 
	 HashSet <String> firstFileList = readFilesToHashSet(file1); 						// Read list1.txt and write contents to HashSet
	 HashSet <String> secondFileList = readFilesToHashSet(file2); 					// Read list2.txt and write contents to HashSet
	 HashSet <String> inBothLists = new HashSet<String>(); 								  // helper collection to store results
	 HashSet <String> inFirstListOnly = new HashSet<String>(); 							// helper collection to store results
	 HashSet <String> inSecondListOnly = new HashSet<String>();							// helper collection to store results
	 
	 // Do the actual searching and fill the helper collections
	 searchInOtherList(firstFileList, secondFileList, inFirstListOnly, inBothLists);	
	 searchInOtherList(secondFileList, firstFileList, inSecondListOnly, inBothLists);
	 
	 // create JSON-Arrays of helper collections
	 JsonArrayBuilder inBoLi = Json.createArrayBuilder(inBothLists);
	 JsonArrayBuilder inFiLi = Json.createArrayBuilder(inFirstListOnly);
	 JsonArrayBuilder inSeLi = Json.createArrayBuilder(inSecondListOnly);
	 
	 // add the created JSON-Arrays to a container JSON Object
	 JsonObjectBuilder obj = Json.createObjectBuilder()
			 					 .add("onlyInList1", inFiLi)
			 					 .add("onlyInList2", inSeLi)
			 					 .add("inBothLists", inBoLi);
	 // build the container JSON Object
	 JsonObject output= obj.build();
	 
	 // Format the output
	 Map<String, Boolean> config = new HashMap<>();
	 config.put(JsonGenerator.PRETTY_PRINTING, true);
	 JsonWriterFactory writerFactory = Json.createWriterFactory(config);
	 
	 // print the result to the console
	 writerFactory.createWriter(System.out).write(output);

 }
 
 //=======================================================================
 /**
  * Method to read a text file and store its contents in a HashSet.
  * @param file
  * @return HashSet<String>
  */
 public static HashSet<String> readFilesToHashSet(Path file) {
	 HashSet<String> list = new HashSet<String>();
	 BufferedReader br=null;
	 try {
		 br = new BufferedReader(new FileReader(file.toFile()));
		 for (String temp = br.readLine();temp!=null;temp=br.readLine())
			 list.add(temp);
		 br.close();
	 } catch (IOException e) {
		 e.printStackTrace();
	 } finally {
	   try {
	     if(br!=null)
	       br.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
	 }
	 return list;
 }
 
 //=======================================================================
 /**
  * Method to iterate through a HashSet and look whether its contents are also present in another HashSet. If so, the duplicate elements are stored in another HashSet called "bothLists". Unique values are stored in a HashSet called "thisListOnly"
  * @param thisList
  * @param otherList
  * @param thisListOnly
  * @param bothLists
  */
 public static void searchInOtherList(HashSet<String> thisList, HashSet<String> otherList, HashSet<String> thisListOnly, HashSet<String> bothLists) {
	 Iterator<String> it = thisList.iterator();
	 while(it.hasNext()) {
		 String searchElement = it.next();
		 if(otherList.contains(searchElement)) {
			 bothLists.add(searchElement);
		 } else {
			 thisListOnly.add(searchElement);
		 }
	 }
 }
}
