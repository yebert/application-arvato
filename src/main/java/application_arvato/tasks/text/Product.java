package application_arvato.tasks.text;


import java.nio.file.Path;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
//Aufgabe 2

/**
 * This class respresents the structure of Products.
 * @author Yannis Alexander Ebert
 *
 */
public class Product {
  //fields
  String name;
  String countryOfOrigin;
  double price;
  boolean isFragile;
  int timesPurchased;
  //=======================================================================
  // Constructor
  public Product(String name, String countryOfOrigin, double price, boolean isFragile, int timesPurchased) {
    super();
    this.name = name;
    this.countryOfOrigin = countryOfOrigin;
    this.price = price;
    this.isFragile = isFragile;
    this.timesPurchased = timesPurchased;
  }
  //=======================================================================
  // Getters and setters
  public String getName() {
    return name;
  }


  public void setName(String name) {
    this.name = name;
  }


  public String getCountryOfOrigin() {
    return countryOfOrigin;
  }


  public void setCountryOfOrigin(String countryOfOrigin) {
    this.countryOfOrigin = countryOfOrigin;
  }


  public double getPrice() {
    return price;
  }


  public void setPrice(double price) {
    this.price = price;
  }


  public boolean isFragile() {
    return isFragile;
  }


  public void setFragile(boolean isFragile) {
    this.isFragile = isFragile;
  }


  public int getTimesPurchased() {
    return timesPurchased;
  }


  public void setTimesPurchased(int timesPurchased) {
    this.timesPurchased = timesPurchased;
  }

  //=======================================================================
  // toString()
  @Override
  public String toString() {
    return "Product [name=" + name + ", countryOfOrigin=" + countryOfOrigin + ", price=" + price + ", isFragile="
        + isFragile + ", timesPurchased=" + timesPurchased + "]";
  }

  //=======================================================================
/**
 * This Method builds a JSON-Object of a Product
 * @return JsonObject
 */
  public JsonObject generateProductJSON() {
    JsonObjectBuilder obj = Json.createObjectBuilder()
        .add("name", name)
        .add("countryOfOrigin", countryOfOrigin)
        .add("price", price)
        .add("isFragile", isFragile)
        .add("timesPurchased", timesPurchased);
    return obj.build();
  }
  //=======================================================================
  /**
   * This method creates a Json-Array of 0-n Products and writes it to a new Json-file.
   * @param fileName
   * @param products
   * @return Path fileDestination
   */
  public static Path createProductsJSONFile(String fileName, Product...products) {
    JsonArrayBuilder jsonArray = Json.createArrayBuilder();
    for(Product pr : products) {
      jsonArray.add(pr.generateProductJSON());
    }
    JsonArray jArr = jsonArray.build();
    return Analyse.buildJSONFile(fileName, jArr);
    
  }
}
