package application_arvato.tasks.text;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonStructure;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonParser;
// Aufgabe 2
/**
 * Anders als in der Aufgabe vorgesehen läuft die Analyse der Json-Datei nicht über eine Webanwendung. 
 * Ich habe bisher leider noch kaum Erfahrung in der Einrichtung von REST(ful) Webservices und Java EE 
 * und die vorgegebene Zeit ließ mir neben meiner Weiterbildung keine Möglichkeit der adäquaten Einarbeitung.
 * Die im Aufgaben-Szenario angegebene Json-Datei-Übergabe habe ich mittels der Product-Methode createProductsJsonFile "simuliert".
 * @author Yannis Alexander Ebert
 *
 */
public class Analyse {
  
  //=======================================================================
  /**
   * This method takes a Product-Json-File-Path and parses its contents to new Product instances. These instances are stored in a List<Product>
   * @param file
   * @return List<Product> productList
   */
  public static List<Product> readProductJsonFileToList(Path file){
    List<Product> productList = new ArrayList<>();
    try {
      JsonParser parser = Json.createParser(Files.newBufferedReader(file));
      parser.next();
      JsonArray arr = parser.getArray();
      
      for(Object o : arr) {
        JsonObject product = (JsonObject)o;
        String name = product.getString("name");                              
        String countryOfOrigin = product.getString("countryOfOrigin");
        double price = product.getInt("price");                               // PROBLEM/FEHLER: abgeschnittene Kommazahl - falsche Rundung!
        boolean isFragile = product.getBoolean("isFragile");
        int timesPurchased = product.getInt("timesPurchased");
        Product p = new Product(name,countryOfOrigin,price,isFragile,timesPurchased);
        productList.add(p);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return productList;
  }
  
  //=======================================================================
  /**
   * 
   * @param jsonFile
   */
  public static void analyzeProductsJSONFile (Path jsonFile) {
    //Declaration of local variables
    List<Product> productList=readProductJsonFileToList(jsonFile);
    Product maxValueProduct;Product minValueProduct;Product mostPopularProduct;
    boolean fragile=false;
    
    //=======================================================================
    // The actual analyzation
    
    // finds the most expensive product
    maxValueProduct= productList.stream().max((x,y)->((Double)x.getPrice()).compareTo(((Double)y.getPrice()))).get(); 
    
    // finds the cheapest product
    minValueProduct= productList.stream().min((x,y)->((Double)x.getPrice()).compareTo(((Double)y.getPrice()))).get(); 
    
    // finds the most popular product
    mostPopularProduct= productList.stream().max((x,y)->((Integer)x.getTimesPurchased()).compareTo(((Integer)y.getTimesPurchased()))).get(); 
    
    // collects all german Products
    List<Product> germanProducts = productList.stream().filter(x->x.getCountryOfOrigin().equals("DE")).collect(Collectors.toCollection(()->{return new ArrayList<Product>();}));
    
    // collects all chinese Products
    List<Product> chineseProducts = productList.stream().filter(x->x.getCountryOfOrigin().equals("CN")).collect(Collectors.toCollection(()->{return new ArrayList<Product>();}));
    
    //checks whether there is a Product, that is fragile
    Optional<Product> fragilProduct = productList.stream().filter(x->x.isFragile()==true).findAny();
    if(fragilProduct.isPresent()) fragile=true;
    
    //=======================================================================
    // Not the best solution. Store the productnames of germanProducts and chineseProducts in two further Lists in order to add them to the Json-Arrays.
    List<String> germanNames = new ArrayList<>();
    List<String> chineseNames = new ArrayList<>();
    for (Product p: germanProducts) {
      String name = p.getName();
      germanNames.add(name);
    }
    for (Product p: chineseProducts) {
      String name = p.getName();
      chineseNames.add(name);
    }
    
    // Create Json-Objects and Json-Arrays of the analyzed results and store them in a response.json File
    JsonArrayBuilder gP = Json.createArrayBuilder(germanNames);
    JsonArrayBuilder cP = Json.createArrayBuilder(chineseNames);
    JsonObjectBuilder responseBuilder = Json.createObjectBuilder()
                                            .add("mostExpensiveProduct", maxValueProduct.getName())
                                            .add("cheapestProduct", minValueProduct.getName())
                                            .add("mostPopularProduct", mostPopularProduct.getName())
                                            .add("germanProducts", gP)
                                            .add("chineseProducts",cP)
                                            .add("containsFragileProducts", fragile);
    JsonObject response = responseBuilder.build();
    buildJSONFile("response.json", response);
  }
  
  //=======================================================================
  /**
   * This method creates a new Json-File and stores the passed JsonStructure in it.
   * @param fileName
   * @param obj
   * @return Path of built Json-File
   */
  public static Path buildJSONFile (String fileName, JsonStructure obj) {
    //Formatting
    Map<String, Boolean> config = new HashMap<>();
    config.put(JsonGenerator.PRETTY_PRINTING, true);
    JsonWriterFactory writerFactory = Json.createWriterFactory(config);
    
    File output = new File(System.getProperty("user.dir")+"/"+fileName);
    //write JsonStructure to File
    try (BufferedWriter bw = new BufferedWriter(new FileWriter(output))){
      output.createNewFile();
      writerFactory.createWriter(bw).write(obj);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return output.toPath();
  }
  
  //=======================================================================
  // main-Method for testing
  public static void main(String[] args) {
    Path analyse = Product.createProductsJSONFile("sampleProductsData.json",new Product("product1","DE",123.50f,true,150), new Product("product2","DE",45f,false,1241), new Product("product3","ES",13f,false,772));
    analyzeProductsJSONFile(analyse);
  }
}
